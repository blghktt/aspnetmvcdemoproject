﻿CREATE TABLE [dbo].[Car]
(
	[VIN] CHAR(17) NOT NULL PRIMARY KEY, 
    [Model] NVARCHAR(50) NULL, 
    [ModelYear] INT NULL, 
    [Price] DECIMAL(18, 3) NULL, 
    [Color] NVARCHAR(50) NULL, 
    [Manufacturer_id] INT NOT NULL, 
    [Owner_id] INT NULL, 
    CONSTRAINT [FK_Car_Manufacturer] FOREIGN KEY ([Manufacturer_id]) REFERENCES [dbo].[Manufacturer]([Id]), 
    CONSTRAINT [FK_Car_Owner] FOREIGN KEY ([Owner_id]) REFERENCES [dbo].[Owner]([Id])
)
