﻿CREATE TABLE [dbo].[Manufacturer]
(
	[Id] INT IDENTITY (1, 1) NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [Country] NVARCHAR(50) NULL
)
