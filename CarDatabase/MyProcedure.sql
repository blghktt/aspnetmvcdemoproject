﻿CREATE PROCEDURE [dbo].[MyProcedure]
	@gender int = 2
AS
BEGIN
    SELECT m.Country AS 'Country', COUNT(*) AS 'Count'
	FROM Manufacturer m
	JOIN Car c
	ON m.Id = c.Manufacturer_id
	JOIN Owner o
	ON o.Id = c.Owner_id
	WHERE o.Gender = @gender
	GROUP BY m.Country
	ORDER BY m.Country DESC
END