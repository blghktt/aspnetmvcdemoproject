﻿CREATE TABLE [dbo].[Owner]
(
	[Id] INT IDENTITY (1, 1) NOT NULL PRIMARY KEY, 
    [LastName] NVARCHAR(50) NOT NULL, 
    [FirstName] NVARCHAR(50) NULL, 
    [DateOfBirth] DATE NULL, 
    [Gender] INT NULL
)
