﻿DELETE FROM Car;
DELETE FROM Manufacturer;
INSERT INTO Manufacturer VALUES 
        ('Honda', 'Japan'), 
        ('Ford', 'USA'), 
        ('Toyota', 'Japan'),
		('Mazda', 'Japan'), 
        ('Ferrari', 'Italy'), 
        ('Porsche', 'Germany'),
		('Opel', 'Germany'), 
        ('Peugeot', 'France'), 
        ('Suzuki', 'Japan'),
		('Alfa Romeo', 'Italy'), 
        ('Mahindra', 'India'), 
        ('ZiL', 'Russia'),
		('Puli', 'Hungary');

DELETE FROM Owner;
INSERT INTO Owner VALUES 
        ('Kiss', 'Eszter', '20001009', 2),
        ('Nagy', 'Sándor', '19700118', 1),
		('Farkas', 'Anna', '19860304', 2),
        ('Nagy', 'Imola', '19660606', 2),
		('Nagy', 'Barbara', '19991111', 2),
        ('Vicze', 'Ferenc', '19900429', 1),
		('Lukács', 'Szabolcs', '19920930', 1),
        ('Lakatos', 'Lilla', '19880518', 2),
		('Vass', 'Fanni', '19960831', 2);

INSERT INTO Car VALUES
('1FAFP34N15W314893','Mondeo',2003,750000,'Silver',(SELECT id from manufacturer where name = 'Ford'), NULL),

('5XYKT3A61CG258031','Goa',2010,11000000,'Red',(SELECT id from manufacturer where name = 'Mahindra'), (SELECT id from owner where FirstName = 'Imola')),
('1JCWL7742FT124215','Focus',2000,4560000,'Black',(SELECT id from manufacturer where name = 'Ford'), NULL),
('1GCHC23U81F110894','Swift',2015,15000000,'White',(SELECT id from manufacturer where name = 'Suzuki'), (SELECT id from owner where FirstName = 'Eszter')),

('1B4HS28Z7YF247257','RX-8',2003,1750000,'Orange',(SELECT id from manufacturer where name = 'Mazda'), (SELECT id from owner where FirstName = 'Ferenc')),
('1FMSU41F0YEC49285','Mustang',1995,1000000,'Red',(SELECT id from manufacturer where name = 'Ford'), NULL),
('1GTZ7UCA7E1214712','Astra',2001,550000,'RedOrange',(SELECT id from manufacturer where name = 'Opel'), (SELECT id from owner where FirstName = 'Szabolcs')),
('1GAHG35K0RF123469','Vectra',2001,895000,'Blue',(SELECT id from manufacturer where name = 'Opel'), NULL),
('1HD1CAP14XK158046','Mondeo',2000,400000,'ViridianGreen',(SELECT id from manufacturer where name = 'Ford'), NULL),

('1FDSE34L08DA17607','6',2015,2500000,'Black',(SELECT id from manufacturer where name = 'Mazda'), (SELECT id from owner where FirstName = 'Anna')),
('1GCZGUCL6F1124342','Yaris',2010,1010000,'DarkBlue',(SELECT id from manufacturer where name = 'Toyota'), NULL),
('WAU4FAFR4AA014911','Avensis',2014,4500000,'Blue',(SELECT id from manufacturer where name = 'Toyota'), NULL),
('1GDL6P1G2FV527383','Civic',2014,5000000,'Silver',(SELECT id from manufacturer where name = 'Honda'), (SELECT id from owner where FirstName = 'Fanni')),
('2GCEC19H5R1335008','Jazz',2016,6500000,'Green',(SELECT id from manufacturer where name = 'Honda'), (SELECT id from owner where FirstName = 'Lilla')),
('1GCEC14V74E225775','Getz',2017,10000000,'Purple',(SELECT id from manufacturer where name = 'Honda'), NULL),

('3GCPK9E32BG172870','GTB Fiorano',2010,35000000,'Ebony',(SELECT id from manufacturer where name = 'Ferrari'), NULL),
('1B7GG2AZ11S167668','206',2002,1000000,'Pink',(SELECT id from manufacturer where name = 'Peugeot'), (SELECT id from owner where FirstName = 'Lilla')),
('JN8AS5MT3FW110888','206',2007,6550000,'DeepAquamarine',(SELECT id from manufacturer where name = 'Peugeot'), (SELECT id from owner where FirstName = 'Ferenc')),
('1GKGC26U34R062350','307',2010,7000000,'Citron',(SELECT id from manufacturer where name = 'Peugeot'), (SELECT id from owner where FirstName = 'Ferenc')),
('WA1LGBFE7FD072008','Mondeo',2001,550000,'Black',(SELECT id from manufacturer where name = 'Ford'), NULL),
('1GDG7H1P6MJ579236','Mondeo',2006,1250000,'ViridianGreen',(SELECT id from manufacturer where name = 'Ford'), NULL),
('1GTJC39G24E308356','Yaris',2003,650000,'Red',(SELECT id from manufacturer where name = 'Toyota'), NULL),

('1XMDC9938EK136927','Swift',2003,880000,'RedOrange',(SELECT id from manufacturer where name = 'Suzuki'), NULL),
('1FMRE11W21HB62714','Escort',2009,1350000,'Silver',(SELECT id from manufacturer where name = 'Ford'), (SELECT id from owner where FirstName = 'Eszter')),
('JHMED3559KS089377','Edge',2004,760000,'Silver',(SELECT id from manufacturer where name = 'Ford'), (SELECT id from owner where FirstName = 'Imola')),
('KNALD124165015252','Galaxy',2007,950000,'Almond',(SELECT id from manufacturer where name = 'Ford'), NULL),
('1GDJ7C1C8XJ514687','Mondeo',2008,750000,'Black',(SELECT id from manufacturer where name = 'Ford'), NULL),
('ZHWGU8AJ9CLA09401','Vitara',2011,4750000,'Silver',(SELECT id from manufacturer where name = 'Suzuki'), (SELECT id from owner where FirstName = 'Ferenc')),

('1XPFDU9X7YD513318','Swift',2002,550000,'Silver',(SELECT id from manufacturer where name = 'Suzuki'), NULL),
('1FAFP58S9XG261031','Pinguin',2000,200000,'CatalinaBlue',(SELECT id from manufacturer where name = 'Puli'), NULL),
('2FZACGDC86AW61929','Ignis',1999,450000,'White',(SELECT id from manufacturer where name = 'Suzuki'), (SELECT id from owner where FirstName = 'Szabolcs')),
('2FZSAZCV96AW04700','Cascada',1997,390000,'Black',(SELECT id from manufacturer where name = 'Opel'), NULL),

('1B7GL26X2KS129788','Fiesta',2006,1000000,'Saffron',(SELECT id from manufacturer where name = 'Ford'), NULL),
('1FMCU02B53KB44163','308',2003,1550000,'RedOrange',(SELECT id from manufacturer where name = 'Peugeot'), NULL),
('1GCEK14H9SZ110484','Corsa',2008,1450000,'Red',(SELECT id from manufacturer where name = 'Opel'), NULL),
('KM4AG43A7V1141061','Corsa',2008,1950000,'Silver',(SELECT id from manufacturer where name = 'Opel'), NULL);

