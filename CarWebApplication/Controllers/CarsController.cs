﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CarWebApplication.Models;

namespace CarWebApplication.Controllers
{
    public class CarsController : Controller
    {
        private CarDatabaseEntities1 db = new CarDatabaseEntities1();

        // GET: Cars
        public ActionResult Index()
        {
            var cars = db.Cars.Include(c => c.Manufacturer).Include(c => c.Owner);
            return View(cars.ToList());
        }

        // GET: Cars/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Car car = db.Cars.Find(id);
            if (car == null)
            {
                return HttpNotFound();
            }
            return View(car);
        }

        // GET: Cars/Create
        public ActionResult Create()
        {
            ViewBag.Manufacturer_id = new SelectList(db.Manufacturers, "Id", "Name");
            ViewBag.Owner_id = new SelectList(new List<Owner> { null }.Concat(db.Owners.OrderBy(e => e.LastName).ThenBy(e => e.FirstName).ToList()), "Id", "FullName");
            return View();
        }

        // POST: Cars/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "VIN,Model,ModelYear,Price,Color,Manufacturer_id,Owner_id")] Car car)
        {
            if (ModelState.IsValid)
            {
                db.Cars.Add(car);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Manufacturer_id = new SelectList(db.Manufacturers, "Id", "Name", car.Manufacturer_id);
            ViewBag.Owner_id = new SelectList(new List<Owner> { null }.Concat(db.Owners.OrderBy(e => e.LastName).ThenBy(e => e.FirstName).ToList()), "Id", "FullName", car.Owner_id);
            return View(car);
        }

        // GET: Cars/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Car car = db.Cars.Find(id);
            if (car == null)
            {
                return HttpNotFound();
            }
            ViewBag.Manufacturer_id = new SelectList(db.Manufacturers, "Id", "Name", car.Manufacturer_id);
            ViewBag.Owner_id = new SelectList(new List<Owner> { null }.Concat(db.Owners.OrderBy(e => e.LastName).ThenBy(e => e.FirstName).ToList()), "Id", "FullName", car.Owner_id);
            return View(car);
        }

        // POST: Cars/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "VIN,Model,ModelYear,Price,Color,Manufacturer_id,Owner_id")] Car car)
        {
            if (ModelState.IsValid)
            {
                db.Entry(car).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Manufacturer_id = new SelectList(db.Manufacturers, "Id", "Name", car.Manufacturer_id);
            ViewBag.Owner_id = new SelectList(new List<Owner> { null }.Concat(db.Owners.OrderBy(e => e.LastName).ThenBy(e => e.FirstName).ToList()), "Id", "FullName", car.Owner_id);
            return View(car);
        }

        // GET: Cars/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Car car = db.Cars.Find(id);
            if (car == null)
            {
                return HttpNotFound();
            }
            return View(car);
        }

        // POST: Cars/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Car car = db.Cars.Find(id);
            db.Cars.Remove(car);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
