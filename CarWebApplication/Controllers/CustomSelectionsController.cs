﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CarWebApplication.Models;

namespace CarWebApplication.Controllers
{
    public class CustomSelectionsController : Controller
    {
        private CarDatabaseEntities1 db = new CarDatabaseEntities1();

        // GET: CustomSelections
        public ActionResult Index()
        {
            var resultByLinq = db.Cars
                .Include(c => c.Manufacturer).Include(c => c.Owner)
                .Where(c => c.Owner.Gender == 2)
                .GroupBy(c => c.Manufacturer.Country)
                .Select(gr => new CountryCountModel { Country = gr.Key, Count = gr.ToList().Count })
                .OrderByDescending(gr => gr.Country).ToList();

            var resultByStoredProcedure = db.MyProcedure(2).Select(e => new CountryCountModel { Country = e.Country, Count = e.Count }).ToList();

            return View(new List<IEnumerable<CountryCountModel>> { resultByLinq, resultByStoredProcedure });
        }
    }
}