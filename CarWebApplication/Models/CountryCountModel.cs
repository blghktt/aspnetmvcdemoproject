﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarWebApplication.Models
{
    public class CountryCountModel
    {
        public string Country { get; set; }

        public int? Count { get; set; }
    }
}